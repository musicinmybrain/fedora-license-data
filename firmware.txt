# This file is for collecting the actual text found in Fedora
# packages for firmware. In Callaway system usually:
#  Redistributable, no modification permitted
#  Freely redistributable without restrictions
# We use SPDX expression, "LicenseRef-Fedora-Firmware"
# See the instructions at https://docs.fedoraproject.org/en-US/legal/update-existing-packages/#_freely_redistributable_without_restrictions
#
# Include the following information:
#
# Fedora package name
#
# Location of where you found the license notice text.
# Preferably this would be a direct link to a file. If that is not possible,
# provide enough information such that someone else can find the text in the wild
# where you found it.
#
# The actual text found that allow redistribution..
#
# Copy template below and add yours to top of list, adding a space between entries.

package = atmel-firmware
location = http://www.thekelleys.org.uk/atmel/atmel-firmware-1.3.tar.gz
text = '''
Copyright 2004 Atmel Corporation. All Rights Reserved. Redistribution                                                                                               
and use of the microcode software ("Firmware") is permitted provided
that the following conditions are met:

    1. Firmware is redistributed in object code only.

    2. Any reproduction of Firmware must contain the above copyright
       notice, this list of conditions and the below disclaimer in the
       documentation and/or other materials provided with the
       distribution; and

    3. The name of Atmel Corporation may not be used to endorse or
       promote products derived from this Firmware without specific
       prior written consent.

DISCLAIMER: ATMEL PROVIDES THIS FIRMWARE "AS IS" WITH NO WARRANTIES OR
INDEMNITIES WHATSOEVER. ATMEL EXPRESSLY DISCLAIMS ANY EXPRESS, STATUTORY
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NON-INFRINGEMENT. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS FIRMWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

USER ACKNOWLEDGES AND AGREES THAT THE PURCHASE OR USE OF THE FIRMWARE
WILL NOT CREATE OR GIVE GROUNDS FOR A LICENSE BY IMPLICATION, ESTOPPEL,
OR OTHERWISE IN ANY INTELLECTUAL PROPERTY RIGHTS (PATENT, COPYRIGHT,
TRADE SECRET, MASK WORK, OR OTHER PROPRIETARY RIGHT) EMBODIED IN ANY
OTHER ATMEL HARDWARE OR FIRMWARE EITHER SOLELY OR IN COMBINATION WITH
THE FIRMWARE.
'''

package = microcode_ctl
location = https://raw.githubusercontent.com/intel/Intel-Linux-Processor-Microcode-Data-Files/main/license
text = '''
Copyright (c) 2018-2021 Intel Corporation.
All rights reserved.

Redistribution.

Redistribution and use in binary form, without modification, are permitted,
provided that the following conditions are met:

1.  Redistributions must reproduce the above copyright notice and the
    following disclaimer in the documentation and/or other materials provided
    with the distribution.

2.  Neither the name of Intel Corporation nor the names of its suppliers may
    be used to endorse or promote products derived from this software without
    specific prior written permission.

3.  No reverse engineering, decompilation, or disassembly of this software
    is permitted.


"Binary form" includes any format that is commonly used for electronic
conveyance that is a reversible, bit-exact translation of binary
representation to ASCII or ISO text, for example "uuencode".

DISCLAIMER.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
'''

package = bcm283x-firmware
location = https://github.com/raspberrypi/firmware/blob/master/boot/LICENCE.broadcom
text = '''
Copyright (c) 2006, Broadcom Corporation.
Copyright (c) 2015, Raspberry Pi (Trading) Ltd
All rights reserved.

Redistribution.  Redistribution and use in binary form, without
modification, are permitted provided that the following conditions are
met:

* This software may only be used for the purposes of developing for, 
  running or using a Raspberry Pi device, or authorised derivative
  device manufactured via the element14 Raspberry Pi Customization Service
* Redistributions must reproduce the above copyright notice and the
  following disclaimer in the documentation and/or other materials
  provided with the distribution.
* Neither the name of Broadcom Corporation nor the names of its suppliers
  may be used to endorse or promote products derived from this software
  without specific prior written permission.

DISCLAIMER.  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.
'''

package = iscan-firmware
# see spec of iscan-firmware on how to retrieve it
location = iscan-plugin-gt-1500-2.2.0-1.x86_64.rpm
text = '''
                         AVASYS PUBLIC LICENSE
                              2008-04-01

  This License applies to any program or other work identified as such
  at the point of distribution and/or in a suitable location in the
  sources for a work including it, for example in a README file.  Such
  sources should include a verbatim copy of this License.

  The "Program", below, refers to any program or work covered by this
  License; each "Licensee" is addressed as "you".

  You may use, reproduce, modify and distribute the Program subject to
  the terms and conditions below.


    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  1. Copyright of the Program is reserved by AVASYS CORPORATION and
     its Licensor(s).

  2. You may freely reproduce and distribute verbatim copies of the
     Program in any medium, provided that recipients of such copies
     are given a copy of this License.  Verbatim copies are covered
     by the terms of this License.

  3. You may modify the Program and freely distribute your modified
     version(s), provided that you distribute it under the terms of
     this License.  Recipients of any modified version(s) should be
     provided with a copy of this License.

  4. You shall treat those parts of the Program that were provided
     to you in executable or object code only as the proprietary
     and confidential information of AVASYS CORPORATION and its
     Licensor(s).

  5. You may neither reverse engineer, reverse compile, reverse
     assemble nor otherwise attempt to analyse those parts of the
     Program that were provided to you in executable or object code
     only.  However, as a special exception AVASYS CORPORATION and
     its Licensor(s) give permission to reverse engineer the
     Program in those cases, and only those cases, where this is
     required by the terms stipulated in the GNU Library General
     Public License or GNU Lesser General Public License, both as
     published by the Free Software Foundation; either version 2 of
     the former license, version 2.1 of the latter license, or (at
     your option) any later version.

  6. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO
     WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE
     LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING, THE COPYRIGHT
     HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT
     WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT
     NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
     FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS TO THE
     QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE
     PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY
     SERVICING, REPAIR OR CORRECTION.

  7. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN
     WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY
     MODIFY AND/OR REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE
     LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL,
     INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR
     INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS
     OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY
     YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH
     ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN
     ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

                     END OF TERMS AND CONDITIONS
'''

package =
location =
text = '''
text here
'''
